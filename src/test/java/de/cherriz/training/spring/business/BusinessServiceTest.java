package de.cherriz.training.spring.business;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BusinessServiceTest {

    @Test
    public void zinssatz() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        BusinessService service = context.getBean(BusinessService.class);

        Assert.assertNotNull(service);
        Assert.assertEquals("1.25 %", service.getZinssatz());
    }

}