package de.cherriz.training.spring.eis;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class EissortenServiceTest {

    @Test
    public void zitrone() {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        EissortenService service = context.getBean(EissortenService.class);

        Assert.assertNotNull(service);
        Assert.assertEquals("Eissorte: Zitrone für 0.8 € pro Kugel", service.getZitrone());
    }

    @Test
    public void schokolade() {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        EissortenService service = context.getBean(EissortenService.class);

        Assert.assertNotNull(service);
        Assert.assertEquals("Eissorte: Schokolade für 0.8 € pro Kugel", service.getSchokolade());
    }

}
