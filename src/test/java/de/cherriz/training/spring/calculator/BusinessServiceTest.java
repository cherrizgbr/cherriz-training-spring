package de.cherriz.training.spring.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BusinessServiceTest {

    @Test
    public void springWithXML() {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        BusinessService service = context.getBean(BusinessService.class);

        Assert.assertNotNull(service);

        Assert.assertEquals(Long.valueOf(-150), service.performBusinessOperation(5L, 10L, 15L));
    }

    @Test
    public void springWithAppConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        BusinessService service = context.getBean(BusinessService.class);

        Assert.assertNotNull(service);

        Assert.assertEquals(Long.valueOf(-150), service.performBusinessOperation(5L, 10L, 15L));
    }

}