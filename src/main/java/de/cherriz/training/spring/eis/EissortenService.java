package de.cherriz.training.spring.eis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EissortenService {

    private String preis;

    @Autowired
    private Schokolade schokolade;

    @Autowired
    private Zitrone zitrone;

    @Autowired
    public EissortenService(@Value("${preis}") Double preis) {
        this.preis = preis + " € pro Kugel";
    }

    public String getZitrone() {
        return zitrone.toString() + " für " + preis;
    }

    public String getSchokolade() {
        return schokolade.toString() + " für " + preis;
    }

}
