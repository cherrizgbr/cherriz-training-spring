package de.cherriz.training.spring.eis;

import org.springframework.stereotype.Component;

@Component
public class Schokolade {

    @Override
    public String toString(){
        return  "Eissorte: Schokolade";
    }

}