package de.cherriz.training.spring.eis;

import org.springframework.stereotype.Component;

@Component
public class Zitrone {

    @Override
    public String toString(){
        return  "Eissorte: Zitrone";
    }

}