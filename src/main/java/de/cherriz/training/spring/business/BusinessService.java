package de.cherriz.training.spring.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BusinessService {

    private Calculator calc;

    @Value("${zinssatz}")
    private Double zins;

    @Autowired
    public BusinessService(Calculator calc) {
        this.calc = calc;
    }

    public Long performBusinessOperation(Long a, Long b, Long c) {
        return calc.multiply(calc.plus(calc.minus(a, c), calc.divide(a, b)), c);
    }

    public String getZinssatz() {
        return zins + " %";
    }

}
