package de.cherriz.training.spring.business;

public interface Calculator {

    Long plus(Long a, Long b);

    Long minus(Long a, Long b);

    Long multiply(Long a, Long b);

    Long divide(Long a, Long b);

}