package de.cherriz.training.spring.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BusinessService {

    private Calculator calc;

    @Autowired
    public BusinessService(Calculator calc) {
        this.calc = calc;
    }

    public Long performBusinessOperation(Long a, Long b, Long c) {
        return calc.multiply(calc.plus(calc.minus(a, c), calc.divide(a, b)), c);
    }

}
