package de.cherriz.training.spring.calculator;

import org.springframework.stereotype.Component;

@Component
public class CalculatorImpl implements Calculator {

    public Long plus(Long a, Long b) {
        return a + b;
    }

    public Long minus(Long a, Long b) {
        return a - b;
    }

    public Long multiply(Long a, Long b) {
        return a * b;
    }

    public Long divide(Long a, Long b) {
        return a / b;
    }

}