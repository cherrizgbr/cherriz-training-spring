package de.cherriz.training.spring.calculator;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "de.cherriz.training.spring.calculator")
public class ApplicationConfig {

}