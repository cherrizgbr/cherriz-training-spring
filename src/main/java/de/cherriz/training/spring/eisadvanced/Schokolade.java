package de.cherriz.training.spring.eisadvanced;

import org.springframework.stereotype.Component;

@Component
@Eissorte("schokolade")
public class Schokolade implements Eis {

    @Override
    public String toString(){
        return  "Eissorte: Schokolade";
    }

}