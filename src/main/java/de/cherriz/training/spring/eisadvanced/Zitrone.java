package de.cherriz.training.spring.eisadvanced;

import org.springframework.stereotype.Component;

@Component
@Eissorte("zitrone")
public class Zitrone implements Eis {

    @Override
    public String toString(){
        return  "Eissorte: Zitrone";
    }

}